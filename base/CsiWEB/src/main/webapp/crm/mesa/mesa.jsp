<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
%>

<html>
<head>
<title>-- CRM -- Plusoft</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language='javascript' src='/csicrm/webFiles/javascripts/TratarDados.js'></script>

<script type="text/javascript">
	function validaFiltrosEspec() {

		return true;
	}

	function setValoresToForm(ifrm) {
		strTxt = "";
		
		strTxt += "<input type='hidden' name='csNgtbManifEspecMaesVo.0.CS_CDTB_PESSOA_PESS.pess_ds_cgccpf' value='"
			+ document.forms[0].cpf.value + "'>";
		
		ifrm.document.getElementById("camposEspec").innerHTML = strTxt;

	}

	function isDigito(evnt) {

		var tk;
		// Recebe a tela pressionada
		tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode
				: evnt.which;
		if (tk == 9 || tk == 8 || tk == 0 || tk == 46 || tk == 37 || tk == 39
				|| tk == 16 || tk == 17 || tk == 110 || tk == 190) {
			return true;
		}

		if ((!((tk >= 96 && tk <= 105) || (tk >= 48 && tk <= 57))) || tk == 13) {
			(navigator.appName == "Microsoft Internet Explorer") ? event.returnValue = null
					: evnt.returnValue = null;
			return false;
		}
	}

	function iniciar() {
	
	}

</script>
</head>
<body class="principalBgrPageIFRM" onload="iniciar()">
	<html:form action="AbrirMesaBi">
	  
		<table width=100% " border="0" cellspacing="1" cellpadding="1">
		    <tr>
				<td width="1%" class="principalLabel">
					&nbsp;  
				</td>
				<td width="10%" class="principalLabel">
				    Busca CPF:					
				</td>
				<td width="30%" class="principalLabel">
				    &nbsp;
				</td>
				<td width="39%" class="principalLabel">
				    &nbsp;
				</td>
			</tr>
			<tr>
				<td width="1%">
					&nbsp;
				</td>
				<td width="10%">
				    <html:text property="cpf" styleClass="principalObjForm" style="width: 100%;" maxlength="11" />
					
				</td>
				<td width="30%" class="principalLabel">
					&nbsp;
				</td>
				<td width="39%" class="principalLabel">
				    &nbsp;
				</td>
			</tr>
		</table>
	   
	</html:form>
</body>
</html>