<html>
<head>
<title>-- CRM -- Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../csicrm/webfiles/css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


function iniciar(){

	document.forms[0].txtTipoBilhete.value = trataValor(window.dialogArguments.tpBilhete);
	document.forms[0].txtCPF.value = trataValor(window.dialogArguments.cpf);
	document.forms[0].txtCNPJ.value = trataValor(window.dialogArguments.cnpj);
	document.forms[0].txtNOrigem.value = trataValor(window.dialogArguments.numeroDeOrigem);
	document.forms[0].txtNDestino.value = trataValor(window.dialogArguments.numeroDestino);
	document.forms[0].txtStatusSenha.value = trataValor(window.dialogArguments.statussenha);
	document.forms[0].txtStatusCliente.value = trataValor(window.dialogArguments.statuscliente);
	document.forms[0].txtMatricula.value = trataValor(window.dialogArguments.matricula);
	document.forms[0].txtRamal.value = trataValor(window.dialogArguments.ramalDestino);
	document.forms[0].txtProtocolo.value = trataValor(window.dialogArguments.protocolo);
	

	
}

function trataValor(valor){
	if(valor == undefined){
		return "";
	}else{
		return valor;
	}

}



</script>
</head>
<body class="principalBgrPage"  scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="iniciar();">
<form name="formulario">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="16" class="principalPstQuadro">Informa&ccedil;&otilde;es 
            Digitadas na URA</td>
          <td height="16" class="principalQuadroPstVazia">&nbsp;</td>
          <td width="4" height="16" heigt="1"><img src="../../images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" width="100%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
        <tr> 
          <td width="23%" align="right" height="12">Tipo do Bilhete <img src="../../csicrm/webfiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtTipoBilhete" class="principalObjForm" readonly>
          </td>
          <td width="29%" height="12" align="right">Protocolo <img src="../../csicrm/webfiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtProtocolo" class="principalObjForm" readonly>
          </td>
          <td height="12" width="10%">&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
        <tr> 
          <td width="23%" align="right" height="12">CPF <img src="../../csicrm/webfiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtCPF" class="principalObjForm" readonly>
          </td>
          <td width="29%" height="12" align="right">CNPJ <img src="../../csicrm/webfiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtCNPJ" class="principalObjForm" readonly>
          </td>
          <td height="12" width="10%">&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
        <tr> 
          <td width="23%" align="right" height="12">N&ordm; De Origem <img src="../../csicrm/webfiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtNOrigem" class="principalObjForm" readonly>
          </td>
          <td width="29%" height="12" align="right">N&ordm; De Destino <img src="../../csicrm/webfiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtNDestino" class="principalObjForm" readonly>
          </td>
          <td height="12" width="10%">&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
        <tr> 
          <td width="23%" align="right" height="12">Status da Senha <img src="../../csicrm/webfiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtStatusSenha" class="principalObjForm" readonly>
          </td>
          <td width="29%" height="12" align="right">Status do Cliente <img src="../../csicrm/webfiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtStatusCliente" class="principalObjForm" readonly>
          </td>
          <td height="12" width="10%">&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
        <tr> 
          <td width="23%" align="right" height="12">Matr&iacute;cula Funcional 
            <img src="../../images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtMatricula" class="principalObjForm" readonly>
          </td>
          <td width="29%" height="12" align="right">Ramal Destino <img src="../../csicrm/webfiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="19%" height="12"> 
            <input type="text" name="txtRamal" class="principalObjForm" readonly>
          </td>
          <td height="12" width="10%">&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td height="1px" width="4px"><img src="../../csicrm/webfiles/images/linhas/VertSombra.gif" height="100%" width="4"></td>
  </tr>
  <tr> 
    <td height="4" width="100%"><img height="4" width="100%" src="../../csicrm/webfiles/images/linhas/horSombra.gif"></td>
    <td height="4" width="4px"><img src="../../csicrm/webfiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="96%">&nbsp;</td>
    <td width="4%" align="center"><img src="../../csicrm/webfiles/mages/botoes/out.gif" width="25" height="25" class="geralCursoHand" title="Sair" onClick="javascript:window.close();"></td>
  </tr>
</table>
</form>
</body>
</html>
