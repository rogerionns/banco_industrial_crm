<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page
	import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo,br.com.plusoft.csi.adm.helper.MAConstantes,br.com.plusoft.csi.adm.util.Geral"%>

<%
	long funcNrCtiRamal = 0;
	String funcDsCtiAgente = new String();
	String funcDsCtiGrupo = new String();
	String funcDsCtiLogin = new String();
	String funcDsCtiSenha = new String();

	if (session != null
			&& session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
		funcNrCtiRamal = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncNrCtiRamal();
		funcDsCtiAgente = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncDsCtiAgente();
		funcDsCtiGrupo = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncDsCtiGrupo();
		funcDsCtiLogin = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncDsCtiLogin();
		funcDsCtiSenha = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncDsCtiSenha();
	}

	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo) request
			.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>
<title>CTI</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../csicrm/webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="../../csicrm/webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="../../csiweb-gafisa/crm/telefonia/cti.js"></script>

<script language="JavaScript">

	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
		if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
		obj.visibility=v; }
	}

	//parent.onMinRestoreClick(parent.document.getElementById('ifrm01'),parent.document.getElementById('img01'));

</script>

<!--
Metodo necessarios para controle da interface com o cti
-->

<script language="JavaScript">
	
	var countInicio = 0;
	var contadorClickOnList = 0;
	var parametrosRing = "";
	var idPupePendente = ""; //Armazena o codigo da pupe que nao pode ser carregado pois havia um atendimento em curso quando o numero foi entregue pelo cti.
	var anyPendente = ""; //Armazena o numero de telefone que nao pode ser carregado pois havia um em curso
	var comConsulXfer = false;
	//Status do usuario
	var lastStatus = "";
	
	var callMethodsBefore = "";

	var idChamada = '';
	var sistema   = '';
	var idChamadaFinal = '';
	var numeroNaoIdentificado = '';
	var tpBilhete = '';
	var cpf = '';
	var cnpj = '';
	var cnpjcpf = '';
	var statussenha = '';
	var statuscliente = '';
	var matricula = '';
	var numeroDeOrigem = '';
	var numeroDestino = '';
	var ramalDestino = '';
	var protocolo = '';
	var valorResgate = '';
	var tipoResgate = '';
	var tipoRenda = '';
			
	
	//Armazena os nomes dos divs disponiveis
	var listaDivsTelefonia = new Array(); 
	listaDivsTelefonia[0] = 'layerTelefonia';
	listaDivsTelefonia[1] = 'layerLogin';
	listaDivsTelefonia[2] = 'layerDiscagem';
	listaDivsTelefonia[3] = 'layerIndisponibilidade';
	listaDivsTelefonia[4] = 'layerConsTrans';		
	listaDivsTelefonia[5] = 'layerSenha';
	listaDivsTelefonia[6] = 'layerDiscarPara';
	
	//Deixa invisivel  os divs e torna o div visivel apenas o item recebido como parametro
	function showLayerTelefonia(layerName){
		for(var i = 0; i < listaDivsTelefonia.length; i++){
			MM_showHideLayers(listaDivsTelefonia[i], '', 'hide');
		} 
		
		MM_showHideLayers(layerName, '', 'show');
	}
	
	//Metodo utilizado para se logar no cti
	function loginCti(){
	
		//Metodo que esta dentro do cti.js
		//alert(parent.parent.principal.ifrmPessoaTelefonia);
 		if(parent.parent.principal.ifrmPessoaTelefonia == undefined){
 			DHTMLTelefone  = " <table id=\"tableIframeTelefone\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" height=\"100%\"> ";
			DHTMLTelefone += "  	<tr> ";
			DHTMLTelefone += " 	 		<td> ";
			DHTMLTelefone += " 				<iframe name=\"ifrmPessoaTelefonia\" src=\"../../TelefoniaLigacaoRecebida.do?tela=ifrmLigacaoRecebida\" width=\"100%\" height=\"100%\" scrolling=\"no\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" ></iframe> ";
			DHTMLTelefone += " 			</td> "; 					
			DHTMLTelefone += "		</tr> "; 			
			DHTMLTelefone += " </table> ";
			//parent.parent.principal.PessoaTelefonia.innerHTML = DHTMLTelefone;
		} 
		
		if(formulario.telefoniaAgente.value==""){
			alert("O preenchimento do campo agente e obrigatorio.");
			return false;
		}

		var telefoniaIp = formulario.telefoniaIP.value;
		var telefoniaPorta = formulario.telefoniaPorta.value;
		var telefoniaLogin = formulario.telefoniaLogin.value;
		var telefoniaAgente = formulario.telefoniaAgente.value;
		var telefoniaSenha = formulario.telefoniaSenha.value;
		var telefoniaRamal = formulario.telefoniaRamal.value;
		var telefoniaGrupo = formulario.telefoniaGrupo.value;
		var ctiServico = formulario.ctiServico.value;		

		//connect(formulario.telefoniaIP.value,formulario.telefoniaPorta.value,"", formulario.telefoniaLogin.value, formulario.telefoniaAgente.value, formulario.telefoniaSenha.value, formulario.telefoniaRamal.value, formulario.telefoniaGrupo.value,formulario.ctiServico.value);

		connectEspec(telefoniaIp,telefoniaPorta,"",telefoniaAgente,telefoniaAgente,telefoniaSenha,telefoniaRamal,telefoniaGrupo,ctiServico);

		comConsulXfer = false;
		document.all.item('btLogout').disabled = false;
		document.all.item('btLogin').disabled = true;
		alert('Conectado');
		}
	
	
	//Metodo chamado no metodo onLogin
	function onLoginImpl(obj){
		getStatus();
		showLayerTelefonia('layerTelefonia');
		/*
		try{
			parent.ifrmGravador.tcpConnect(formulario.hostGravador.value,formulario.portaGravador.value,formulario.telefoniaRamal.value);
			parent.ifrmGravador.clientLogin("ag" + formulario.telefoniaAgente.value,formulario.telefoniaAgente.value,formulario.telefoniaRamal.value);
		}catch(e){}
		*/
	}
	
	//Metodo utilizado para discar para o telefone
	function makeCallImpl(phone){
		makeCall(phone);
	}
	
	//Metodo utilizado quando o evento o onMakeCall eh executado
	function onMakeCallImpl(obj){
	}
	
	//Metodo executado quando o servidor cti tornar o cliente ready
	function onReadImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o servidor cti tornar o cliente notReady
	function onNotReadyImpl(obj){
		getStatus();
		showLayerTelefonia('layerTelefonia');
	}
	
	//Metodo executado quando o usuario se desloga da aplicacao
	function onLogoutImpl(obj){		
		disconnect();
	}
	
	//Metodo executado quando o usuario se desconecta do servidor
	function onDisconnectImpl(obj){	
		getStatus();			
	}
	
	//Metodo executado quando o usuario ou o servidor desliga a ligacao
	function dropImpl(cDesc,IDChat){
			if(tdStatus.innerText != "Desconectado"){
				if (confirm("Deseja realmente desligar ?")){
					drop(cDesc,IDChat);
					comConsulXfer = false;
				}
			}
	}
	
	//Metodo executado quando o usuario ou o servidor desliga a ligacao
	function onDropImpl(obj){

		getStatus();
	}
	
	//Metodo executado para obter os usuarios que estao conectados no cti
	function getUsersImpl(){
		getUsers();
	}
	
	//Metodo executado quando o usuario executar o metodo getUsersImpl()
	function onGetUsersImpl(obj){
		/*
		var combo = formulario['csCdtbGrupotelGrteVo.idGrteCdGrupotel'];
	
		while(combo.length >= 1){
			combo.remove(0);
		}
	
		var itemVazio = new Option();
		itemVazio.text = "-- Selecione uma op��o --";
		itemVazio.value = "";
		combo.add(itemVazio);
		itemVazio = null;


		var qtd = obj.get("getUsersQtd");		
		for (var i = 1; i <= qtd; i++){
			var novoOpt = new Option();
			
			var nome = obj.get("getUsersName" + i);
			var ramal = obj.get("getUsersRamal" + i);
			
			novoOpt.text = nome + "  (" + ramal + ")";
			novoOpt.value = ramal;
			combo.add(novoOpt);
			novoOpt = null;
			
		}
		*/
	}
	
	//metodo para cancelar uma consulta
	function cancelConsultImpl(){
		cancelConsult();
		comConsulXfer = false;
		showLayerTelefonia('layerTelefonia');
	}
	
	//Metodo executado quando o usuario executa o metodo cancelaConsult(phone)
	function onCancelaConsultImpl(obj){
		showLayerTelefonia('layerTelefonia');
	}
	
	//Metodo utilizado para consultar um assistente
	//Metodo utilizado para consultar um assistente
	function consultImpl(){
		var cTipo = new Array();
		var cVal = new Array();
	
		if (formulario['idGrteCdGrupotel'].value == '0' && formulario.textRamalTransferencia.value == ''){
			alert('Para fazer uma consulta � necesses�rio informar o ramal ou o usu�rio');
		}else{
		
				
				cTipo[0] = "cParam0";
				cVal[0] = "U"; //"N" = Consulta normal / "U" = "consulta com UUI" 
				cTipo[1] = "cParam1";
				cVal[1] = "CHAM";
				cTipo[2] = "cParam2";
				cVal[2] = ""; 
				
				cTipo[3] = "phone";
				cVal[3] = "";
				
		
			if (formulario.textRamalTransferencia.value != ''){
				cVal[3] = formulario.textRamalTransferencia.value;
				consult2(cTipo,cVal);
				comConsulXfer = true;
			}else{
				cVal[3] = formulario['idGrteCdGrupotel'].value;
				consult2(cTipo,cVal);
				comConsulXfer = true;
			}
		}
		formulario['idGrteCdGrupotel'].value = "0";
		formulario.textRamalTransferencia.value = "";
	}
	
	//Metodo executado quando o usuario executa o metodo consult(phone);
	function onConsultImpl(obj){
		
	}

	
	function conferenceImpl(){
		conference();
	}

	
	//Metodo utilizado para tranferir a ligacao
	function tranferImpl(transferTipo){
		var cTipo = new Array();
		var cVal = new Array();
		
		if(comConsulXfer){
			transferTipo = "T"
		}
		
		if(transferTipo == "T"){
			if (formulario['idGrteCdGrupotel'].value == '' && formulario.textRamalTransferencia.value == ''){
				alert('Para fazer uma transfer�ncia � preciso informar o ramal ou o usu�rio');
			}else{
				cTipo[0] = "cParam0";
				cVal[0] = "T";
				if (formulario.textRamalTransferencia.value != ''){
					transfer(formulario.textRamalTransferencia.value, "",cTipo,cVal);
					comConsulXfer = false;
				}else{
					transfer("", formulario['idGrteCdGrupotel'].value,cTipo,cVal);
					comConsulXfer = false;
				}
			}
			formulario['idGrteCdGrupotel'].value = "0";
			formulario.textRamalTransferencia.value = "";
			
		}else if(transferTipo == "X"){
		
			if (formulario['idGrteCdGrupotel'].value == '0' && formulario.textRamalTransferencia.value == ''){
				alert('Para fazer uma transfer�ncia � preciso informar o ramal ou o usu�rio');
			}else{
				cTipo[0] = "cParam0";
				cVal[0] = "X";
				cTipo[1] = "cParam1";
				cVal[1] = "CHAM";
				cTipo[2] = "cParam2";
				cVal[2] = window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML;
				
				if (formulario.textRamalTransferencia.value != ''){
					transfer(formulario.textRamalTransferencia.value, "",cTipo,cVal);
					comConsulXfer = false;
				}else{
					//transfer("", formulario['idGrteCdGrupotel'].value,cTipo,cVal);
					transfer(formulario['idGrteCdGrupotel'].value,"",cTipo,cVal);
					comConsulXfer = false;
				}
			}
			formulario['idGrteCdGrupotel'].value = "0";
			formulario.textRamalTransferencia.value = "";
			
		}else if(transferTipo == "G"){
			cTipo[0] = "cParam0";
			cVal[0] = "G";
			cTipo[1] = "cParam1";
			cVal[1] = formulario.textCPF.value;
			cTipo[2] = "cParam2";
			cVal[2] = formulario.telefoniaAgente.value;
			
			transfer("49001", "",cTipo,cVal);
		}else if(transferTipo == "C"){
			if(formulario.textSenhaValida.value != ""){
				cTipo[0] = "cParam0";
				cVal[0] = "C";
				cTipo[1] = "cParam1";
				cVal[1] = formulario.textCPF.value;
				cTipo[2] = "cParam2";
				cVal[2] = formulario.telefoniaAgente.value;
				cTipo[3] = "cParam3";
				cVal[3] = formulario.textSenhaValida.value;
				transfer("49002", "",cTipo,cVal);
			}else{
				alert("N�o existe senha cadastrada !");
			}
		}
	}
		
	//Metodo executado quando o usuario executa o metodo transfer(phone);
	function onTransferImpl(obj){
		showLayerTelefonia('layerTelefonia');
		submeteGravarAtendimento();
	}
	
	
	function consultaTransClik(){
		if(tdStatus.innerText != "Desconectado"){
			getUsersImpl();
			showLayerTelefonia('layerConsTrans');
		}
	}
	
	function discarParaImpl(){
		if(tdStatus.innerText != "Desconectado"){
			if (formulario.txtNumeroTelefone.value == ''){
				alert('Para fazer uma liga��o � necesses�rio informar o n�mero desejado');
			}else{
				makeCall(formulario.txtNumeroTelefone.value);
			}
		}
	}
	
	function discarPara(){
			if(tdStatus.innerText != "Desconectado"){
				getUsersImpl();
				showLayerTelefonia('layerDiscarPara');
			}
	}
	
	//Metodo executado para o usuario deixar a ligacao muda
	function muteImpl(){

		if (lastStatus == 'mute'){
			notMute();
		}else{
			mute();
		}
	}
	
	//Metodo executado quando o metodo mute() eh executo
	function onMuteImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o metodo notMute() eh executo
	function onNotMuteImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o usuario deseja colocar o usuario em espera
	function holdImpl(){
		if (lastStatus == 'hold'){
			notHold();
		}else{
			hold();
		}
	}
	
	//Metodo executado quando o usuario o executa o metodo hold();
	function onHoldImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o usuario o executa o metodo notHold();
	function onNotHoldImpl(obj){
		getStatus();
	}
	
	function abrePopup(url){
		showModalDialog(url,parent.parent.principal.pessoa.dadosPessoa,'<%=Geral.getConfigProperty("app.crm.identificao.dimensao",
							empresaVo.getIdEmprCdEmpresa())%>');		
	}

	

	//Metodo executado quando o servidor enviar a informacao de phone tocando
	function onRingingImpl(obj){

		
		
		
	}

	
	//Metodo utilizado para deixar o usuario disponivel ou indisponivel
	function availibleClick(){
		if(tdStatus.innerText != "Desconectado"){
			if (lastStatus == 'notReady'){
				changeStatus(true, '');
			}else{
				showLayerTelefonia('layerIndisponibilidade');
			}
		}
	}
	
	//Muda o status do usuario para disponivel/indisponivel
	function changeStatus(varReady, reason){
	
		if (varReady){
			ready();
		}else{
			if (reason == ""){ return false };
			notReady(reason);

		}
		
		formulario['idMopaMotivopausa'].value = "";
	
	}
	
	//Metodo executado quando o client retorna o status do cliente no servidor
	function onGetStatusImpl(comando){
		lastStatus = comando;
		var resultado = "";
		if (comando == 'connect'){
			resultado = "Conectando...";
		}else if (comando == 'connected'){
			resultado = "Conectado";
		}else if (comando == 'disconnected'){
			resultado = "Desconectado";
		}else if (comando == 'disconnect'){
			resultado = "Desconectado";
		}else if (comando == 'ready'){
			resultado = "Dispon�vel";
		}else if (comando == 'notReady'){
			resultado = "Indisponivel";
		}else if (comando == 'talking'){
			resultado = "Falando";
		}else if (comando == 'talkingChat'){
			resultado = "Falando";
		}else if (comando == 'talkingEmail'){
			resultado = "Falando";
		}else if (comando == 'acw'){
			resultado = "ACW";
		}else if (comando == 'mute'){
			resultado = "Mudo";
		}else if (comando == 'hold'){
			resultado = "Espera";
		}else if (comando == 'dialing'){
			resultado = "Discando";
		}else if (comando == 'ringing'){
			resultado = "Chamando";
		}else if (comando == 'consult'){
			resultado = "Consultando";
		}else if (comando == 'erro'){
			resultado = "N�o executou";
		}
		
		document.getElementById("tdStatus").innerHTML = "<i>" + resultado + "</i>";
		
		//Executa o metodo
		if (callMethodsBefore != ''){
			var x = callMethodsBefore;
			callMethodsBefore = "";
			eval(x);
		}
		
	}
	
	//Metodo executado quando der erro no cti server ou algum erro dentro do applet
	function onErrorImpl(comando){
		document.getElementById("tdStatus").innerHTML = "<i> Erro </i>";
		alert(comando);
	}
	
	
	//Metodo utilizado para enviar a mensagem de chat
	function chatMessageSend(txMensagem, IDChat){		
	}
	
	//Metodo utilizado para mostrar a mensagem recebida pelo usuario atraves do chat
	function chatMessageReceive(IDchat2,mensagem,nick,email,nomeCompleto,newMsg){
	}
	
	//Metodo utilizado para enviar a mensagem de email
	function emailMessageSend(){
	}

	//Metodo utilizado para mostrar a mensagem recebida pelo usuario via email
	function emailMessageReceive(from, to, cc, subject, body, date, user,anexos){
	}
	
	
	function voltarTela(){
	
		//formulario.textNumeroDiscar.value = "";
		formulario['idMopaMotivopausa'].value = "";
		formulario['idGrteCdGrupotel'].value = 0;
		formulario.textRamalTransferencia.value = "";
		formulario.txtNumeroTelefone.value = '';	
	
		showLayerTelefonia('layerTelefonia');
	}	

	function submeteGravarAtendimento(){
	
	}
	
	function fechaAplicacao(){
		/*
		try{
			parent.ifrmGravador.clientLogout("ag" + formulario.telefoniaAgente.value,formulario.telefoniaRamal.value);
			parent.ifrmGravador.tcpDisconnect();
		}catch(e){}
		*/
		if(tdStatus.innerText != "Desconectado"){
			changeStatus(false,'');
			logout();
		}
	}


	function listOnClick(idPupeCdPublicopesquisa){

		parent.parent.document.all.item("Layer1").style.visibility = "visible";		
		//top.superior.contaTempo();		
		var url = "Campanha.do?acao=consultar&tela=ifrmCmbCampanhaPessoa";
		url += "&csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa=" + idPupeCdPublicopesquisa;
		url += "&utilizaTravado=true"; //independente se o registro estiver travado nao deve-se usar mesmo
		
		try {
			top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.location = url;
		}
		catch(e){
			contadorClickOnList++;
			if(contadorClickOnList < 10){
				setTimeout("listOnClick(" + idPupeCdPublicopesquisa + ")",1000);
			}else{
				contadorClickOnList = 0;
			}
		}
		
		parent.comandos.location = 'Chamado.do?acao=horaAtual';
		top.superior.contaTempo();
		
		parent.parent.document.all.item("Layer1").style.visibility = "hidden";			
		
	}

	function verificaPessoaPendente(){
		var timeout;	
		
		//finaliza o registro de ativo no cti para que o operador possa receber uma nova ligacao
		//finalizaResultadoPendente();
		
		//alert("codigo da pupe " + idPupePendente);
		if (idPupePendente!="" && idPupePendente != "0"){		
			
			//aguarda a pagina ser carrega para so assim carregar o publico
			if (parent.parent.principal.pessoa.dadosPessoa.pessoaForm.document.readyState != 'complete') {
				timeout = setTimeout ("verificaPessoaPendente()",100);
			}else {
				try { clearTimeout(timeout); } catch(e){}

				var nPupeAux = idPupePendente;
				idPupePendente = "";
				listOnClick(nPupeAux);
			}
		}

	}
	
	function setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento){
	
		if(idTpResultado > 0){
			if(idTpLog > 0){
				schedulingImpl(idTpLog, dtAgendamento, hrAgendamento)
			}else{
				callResultImpl(idTpResultado)
			}
		
		}
		
		
	}
	
	function schedulingImpl(idTpLog, dtAgendamento, hrAgendamento){

		ifrmOperacoes.location.href="\csiweb-gafisa\crm\TelefoniaEspec.do?acao=publicar&tela=ifrmTelefonia" +
		"&codAcao=1"+
		"&codPlusoft=" + parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value +
		"&idProspect=0"+
		"&dataHoraAgenda=" + dtAgendamento + " " + hrAgendamento;

	}

	function callResultImpl(idTpResultado){

		ifrmOperacoes.location.href="\csiweb-gafisa\crm\TelefoniaEspec.do?acao=publicar&tela=ifrmTelefonia" +
		"&codAcao=3"+
		"&codPlusoft=" + parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value +
		"&idProspect=0"+
		"&dataHoraAgenda=";
		
	}

	function setLastStatus(status){
		lastStatus = status;
	}
	
	function listarSenhas(){
		var idPessoa = 0;
		idPessoa = parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		ifrmLstSenhas.location.href="TelefoniaBMG.do?acao=filtrar&tela=ifrmLstSenhas&idPessCdPessoa=" + idPessoa;
	}
	
	function verificaPessoa(){
		if (parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0"){
			alert("� necess�rio salvar os dados da pessoa antes de continuar.");
			return false;
		}
		showLayerTelefonia('layerSenha');
	}
	
	function desabilitaLogout(){

		//if(document.getElementById("tdStatus").innerText == "Indisponivel"){
		logout();
		document.all.item('btLogout').disabled = true;
		document.all.item('btLogin').disabled = false;
		//}else{
		//	alert("� necess�rio estar em pausa para o Logout!");
		//	showLayerTelefonia('layerIndisponibilidade');
		//}

	}
	
	function inicio(){

		if(document.ctiApplet != undefined){
			try{
				if(ctiApplet.isActive()){
				 	ctiApplet.setLocal(true);
				 	document.getElementById("travaTelefonia").style.visibility="hidden";
				}else{
				 	setTimeout("inicio()",5000);
				}	
			}catch(e){
				countInicio++;
				if(countInicio < 3){
					setTimeout("inicio()",5000);
				}
			}
		}else{
			setTimeout("inicio()",5000); 
		}

	}
	
	function getParametros(){
		return parametrosRing;
	}
	
	function iniciaApplet(){

         try{
	        if(ctiApplet == undefined){
		        var strTxt  = " <applet name='ctiApplet' archive='../../csiweb-gafisa/crm/telefonia/psCtiClient.jar' code='br.com.plusoft.cti.client.gui.CTIApplet' align='absmiddle' width='0%' height='0%' MAYSCRIPT><param name = urlCTI value=''></applet>";	
		        //var strTxt = "bbbbbbb";
		        document.getElementById("objetos").innerHTML = strTxt;
		        document.getElementById("objetos").style.visibility="hidden";
		        document.getElementById("objetos").style.visibility="visible";
                }
        }catch(e){
		        var strTxt  = " <applet name='ctiApplet' archive='/csiweb-gafisa/crm/telefonia/psCtiClient.jar' code='br.com.plusoft.cti.client.gui.CTIApplet' align='absmiddle' width='0%' height='0%' MAYSCRIPT><param name = urlCTI value=''></applet>";	
		        //var strTxt = "aaaaaaa";
		        document.getElementById("objetos").innerHTML = strTxt;
		        document.getElementById("objetos").style.visibility="hidden";
		        document.getElementById("objetos").style.visibility="visible";
        }
	}
	
	function exibePopupCampanha(){
		
		//Se for diferente de null e diferente de vazio
		if (formulario.txtCodigoPupe.value!=null && formulario.txtCodigoPupe.value != undefined && formulario.txtCodigoPupe.value != ""){
		
			contadorClickOnList = 0;
			
			var pupeCampanha = "";
			
			try{
				pupeCampanha = parent.comandos.validaCampanha();
			}catch(e){
				pupeCampanha = "-1"; //Para passar no if
			}
			
			if (pupeCampanha!="" && pupeCampanha!="0"){
				if (pupeCampanha != "-1"){
					alert("Existe um atendimento em andamento, para carregar, grave ou cancele o atendimento atual!");
				}
			}else{
				listOnClick(formulario.txtCodigoPupe.value);
			}	
			
		}
		
	}
	
	function openMotivoPausa(){
		url = "AbreMotivoPausa.do";							
		showModalDialog(url,window,'help:no;Status:NO;dialogWidth:280px;dialogHeight:157px,dialogTop:0px,dialogLeft:200px');
	}
</script>

</head>

<body class="esquerdoBgrPageIFRM" text="#000000"
	onload="iniciaApplet();inicio();">
<html:form action="/TelefoniaEspec.do" styleId="formulario">
	<html:hidden property="tela" />
	<html:hidden property="acao" />
	<html:hidden property="telefoniaIP" />
	<html:hidden property="telefoniaPorta" />
	<html:hidden property="telefoniaLogin" />
	<html:hidden property="telefoniaSenha" />
	<html:hidden property="ctiServico" />
	<html:hidden property="hostGravador" />
	<html:hidden property="portaGravador" />
	<input type="hidden" name="txtCodigoPupe"></input>

	<input type="hidden" name="idChamada" />
	<input type="hidden" name="sistema" />
	<input type="hidden" name="idChamadaFinal" />
	<input type="hidden" name="numeroNaoIdentificado" />
	<input type="hidden" name="tpBilhete" />
	<input type="hidden" name="cpf" />
	<input type="hidden" name="cnpj" />
	<input type="hidden" name="statussenha" />
	<input type="hidden" name="statuscliente" />
	<input type="hidden" name="matricula" />
	<input type="hidden" name="numeroDeOrigem" />
	<input type="hidden" name="numeroDestino" />
	<input type="hidden" name="ramalDestino" />
	<input type="hidden" name="protocolo" />

	<input type="hidden" name="valorResgate" />
	<input type="hidden" name="tipoResgate" />
	<input type="hidden" name="tipoRenda" />


	<div id="travaTelefonia" class="geralLayerDisable"
		style="position: absolute; left: 0px; top: 0px; width: 200px; height: 200px; z-index: 100; background-color: #F3F3F3; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: hidden"></div>

		<div id="layerTelefonia"
			style="position: absolute; left: 0px; top: 0px; width:60px; height: 27px; z-index: 1; visibility: visible">
		<table border="0" cellspacing="1" cellpadding="0" align="center" height="80" width="99%">
	  <tr> 
		<td width="53%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_login.gif" name="btLogin" id="btLogin" width="84" height="22" class="geralCursoHand" onclick="showLayerTelefonia('layerLogin');"></td>
		<td width="47%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_logout.gif" name="btLogout" width="83" height="22" class="geralCursoHand" onclick="desabilitaLogout();"></td>
	  </tr>
	  <tr> 
		<td width="53%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_disponivel.gif" width="84" height="24" class="geralCursoHand" onclick="setLastStatus('');callMethodsBefore='availibleClick()';getStatus();"></td>
		<td width="53%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_hold.gif" width="84" height="24" class="geralCursoHand" onclick="holdImpl();"></td>
	 </tr>
	   <tr> 
			<td width="47%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_desligar.gif" width="83" height="23" class="geralCursoHand" onclick="dropImpl('T');"></td>
	  		<td width="47%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_mudo.gif" width="84" height="23" class="geralCursoHand" onClick="muteImpl();"></td>
	  </tr>
	  <tr> 
		<td height="2" colspan="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_discmanual.gif" width="118" height="22" class="geralCursoHand" onclick="discarPara();"></td>
	  </tr>
	  <tr> 
		<td width="58%" colspan="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_transfer.gif" width="118" height="23" class="geralCursoHand" onclick="consultaTransClik();"></td>
      </tr>
	  

	  <!-- tr> 
		<td height="2" width="40%" class="principalLabelHand" onclick="verificaPessoa();"><img src="webFiles/images/botoes/pt/Assunto_Email.gif" width="20" height="20" class="geralCursoHand" onclick="verificaPessoa();">&nbsp;Senha</img></td>
		<!--td height="2" width="60%" class="principalLabelHand" onclick="exibePopupCampanha();"><img src="webFiles/images/botoes/pt/bt_PreAtendimento.gif" width="20" height="20" class="geralCursoHand" onclick="exibePopupCampanha();">&nbsp;Popup Ativo</img></td-->
	  </tr-->
	  <!--tr> 
		<td colspan="2" class="espacoPqn">&nbsp;</td>
	  </tr-->
	  <tr> 
		<td height="16" colspan="1" class="LabelTel"><b>Status:</b></td>
		<td height="16" id="tdStatus" colspan="1" class="LabelTel" align="left"><i>Desconectado</i></td>
	  </tr>
	  
	  <tr> 
		<td height="3" colspan="2">&nbsp;</td>
	  </tr>
	</table>
		</div>
	<div id="layerLogin"
		style="position: absolute; left: 0px; top: 0px; width: 170px; height: 27px; z-index: 1; visibility: hidden">
	<table border="0" cellspacing="1" cellpadding="0" align="top"
		height="80" width="100%">

		<tr>
			<td height="3" colspan="4" class="LabelTel"><b>Login</b></td>
		</tr>
		<tr>
			<td width="10%" class="LabelTel">Agente
			<td>
			<td width="30%" colspan="3"><input type="text"
				name="telefoniaAgente" class="principalObjForm"
				value="<%=funcDsCtiAgente%>" disabled="disabled" size="10"></input></td>
		</tr>

		<tr>
			<td width="10%" class="LabelTel">Ramal
			<td>
			<td width="30%" colspan="3"><input type="text"
				name="telefoniaRamal" class="principalObjForm"
				value="<%=funcNrCtiRamal%>" size="10"></input></td>
		</tr>
		<tr>
			<td width="10%" class="LabelTel">Grupo
			<td>
			<td width="30%" colspan="3"><input type="text"
				name="telefoniaGrupo" class="principalObjForm"
				value="<%=funcDsCtiGrupo%>" disabled="disabled" size="10"></input></td>
		<tr>
		<tr>
			<td width="5%" class="LabelTel">&nbsp;
			<td>
			<td width="20%"><input type="button" name="buttonLogar" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				value="Logar" class="principalObjForm"
				onclick="loginCti();voltarTela();"></input></td>
			<td width="10%" class="LabelTel">&nbsp;
			<td width="30%"><input type="button" name="buttonVoltar" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				value="Voltar" class="principalObjForm" onclick="voltarTela();"></input>
			</td>
		<tr>
	</table>
	</div>

	<!--
INICIO DA DISPONIBILIDADE
-->
	<div id="layerIndisponibilidade"
		style="position: absolute; left: 0px; top: 0px; width: 170px; height: 27px; z-index: 1; visibility: hidden">
	<table border="0" cellspacing="1" cellpadding="0" align="left"
		height="80" width="100%">

		<tr>
			<td height="3" colspan="4" class="LabelTel"><b>Indisponibilidade</b></td>
		</tr>

		<tr>
			<td width="20%" colspan="4" class="LabelTel">Motivo de
			Indisponibilidade</td>
		</tr>

		<tr>
			<td colspan="4" width="20%" class="LabelTel"><html:select
				property="idMopaMotivopausa" styleClass="principalObjForm">
				<html:option value=""> -- Selecione uma op��o --</html:option>
				<logic:present name="cmbMotivoPausaTel">
					<html:options collection="cmbMotivoPausaTel"
						property="mopaNrMativopausa" labelProperty="mopaDsMotivopausa" />
				</logic:present>
			</html:select></td>
		</tr>
		<tr>
			<td width="20%" class="LabelTel"><input type="button" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				name="buttonIndisponibilizar" value="Indispon�vel"
				class="principalObjForm"
				onclick="changeStatus(false, formulario['idMopaMotivopausa'].value);"></input></td>
			<td width="20%" class="LabelTel">&nbsp;</td>
			<td width="10%" class="LabelTel">&nbsp;</td>
			<td width="20%" class="LabelTel"><input type="button" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				name="buttonVoltar" value="Voltar" class="principalObjForm"
				onclick="voltarTela();"></input></td>
	</table>
	</div>
	<!--
FIM DA DISPONIBILIDADE	
-->

	<!--
INICIO DA TRANFERENCIA
-->
	<div id="layerConsTrans"
		style="position: absolute; left: 0px; top: 0px; width: 170px; height: 27px; z-index: 1; visibility: hidden">
	<table border="0" cellspacing="1" cellpadding="0" align="left"
		height="80" width="100%">

		<tr>
			<td height="3" colspan="4" class="LabelTel"><b>Consulta/Transfer�ncia</b></td>
		</tr>

		<tr>
			<td width="20%" colspan="4" class="LabelTel">Usu�rio/conectados</td>
		</tr>

		<tr>
			<td colspan="4" width="20%" class="LabelTel"><html:select
				property="idGrteCdGrupotel" styleClass="principalObjForm">
				<html:option value="0">-- Selecione uma op��o --</html:option>
				<logic:present name="cmbGrupoTel">
					<html:options collection="cmbGrupoTel" property="grteNrGrupotel"
						labelProperty="grteDsGrupotel" />
				</logic:present>
			</html:select></td>
		</tr>

		<tr>
			<td width="40%" class="LabelTel">Telefone</td>
			<td width="40%" class="LabelTel">&nbsp;</td>
		</tr>

		<tr>
			<td colspan="2" width="40%" class="LabelTel"><input
				type="text" name="textRamalTransferencia" class="principalObjForm"></input>
			</td>
		</tr>

		<tr>
			<td width="20%" class="LabelTel"><input type="button" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				name="buttonConsultar" value=" Consultar " class="principalObjForm"
				onclick="consultImpl();"></input></td>
			<td width="20%" class="LabelTel"><input type="button" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				name="buttonTranferir" value=" Transferir " class="principalObjForm"
				onclick="tranferImpl('X');"></input></td>
			<td width="20%" colspan="2" class="LabelTel"><input
				type="button" name="buttonVoltar" value="Voltar" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				class="principalObjForm" onclick="voltarTela();"></input></td>
		</tr>

		<tr>
			<td colspan="2" class="espacoPqn">&nbsp;</td>
		</tr>
		<tr>
			<td width="20%" class="LabelTel"><input type="button" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				name="buttonCancelar" value=" Recuperar " class="principalObjForm"
				onclick="cancelConsultImpl();"></input></td>
			<td width="80%">&nbsp;
		</tr>
		<tr>
	</table>
	</div>

	<div id="layerDiscarPara"
		style="position: absolute; left: 0px; top: 0px; width: 170px; height: 27px; z-index: 1; visibility: hidden">
	<table border="0" cellspacing="1" cellpadding="0" align="left"
		height="80" width="100%">
		<tr>
			<td colspan="2" class="LabelTel"><b>Discar Para:</b></td>
		</tr>
		<tr>
			<td colspan="2" class="LabelTel"><input type="text"
				name="txtNumeroTelefone" class="principalObjForm"></input></td>
		</tr>

		<tr>
			<td width="50%" align="center" class="LabelTel"><input
				type="button" name="buttonConsultar" value="Discar" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				class="principalObjForm" onclick="discarParaImpl()"></input></td>
			<td width="50%" align="center" class="LabelTel"><input
				type="button" name="buttonVoltar" value="Voltar" style="background:#7EA4C2;font-family:arial;font-size:11;font-weight:bold;"
				class="principalObjForm" onclick="voltarTela();"></input></td>
		</tr>

		<tr>
			<td colspan="2" class="espacoPqn">&nbsp;</td>
		</tr>
	</table>
	</div>




	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>

	<table>
		<tr>
			<td><input name="txtUui" type="text" value=""></input></td>
		</tr>
	</table>

	<td></td>
	<!--
FIM DA TRANFERENCIA	
-->

	<div id="divCab"></div>

	<div id="objetos"
		style="position: absolute; left: 0px; top: 200px; width: 500px; height: 500px; z-index: 1; visibility: visible;">

	</div>

	<iframe name="ifrmOperacoes" id="ifrmOperacoes" width="0%" height="0%"></iframe>

</html:form>

</body>
</html>
