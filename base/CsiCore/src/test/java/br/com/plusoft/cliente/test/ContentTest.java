/*
 * This document contains confidential and proprietary information of Plusoft S/A
 * No use or disclosure shall be made without the written consent of Plusoft S/A
 * All rights reserved.
 *
 * Copyright Plusoft S/A 2011.
 *
 * This notice is not an admission of publication.
 */
package br.com.plusoft.cliente.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import br.com.plusoft.csi.espec.plugin.ValidatorPlugin;


/**
 * Name: ContentTest
 * <p/>
 * Description: Test case for the Content Model object
 * <p/>
 * Author: Hermes
 * Version: 1.0-SNAPSHOT
 * Comments:
 */
public class ContentTest {

    /**
     * Method responsible for test the manipulation of object Content
     *
     * @throws Exception if the test fails
     */
    @Test
    public void testGlobalContent() throws Exception {
      ValidatorPlugin plugin = new ValidatorPlugin();
      assertNotNull(plugin);
      new String();
    }

}
