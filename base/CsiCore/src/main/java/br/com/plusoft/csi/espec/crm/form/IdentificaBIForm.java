package br.com.plusoft.csi.espec.crm.form;

import br.com.plusoft.csi.crm.form.IdentificaForm;

public class IdentificaBIForm extends IdentificaForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	private String pessdscgccpf;

	public void reset(){
		super.reset();
		setPessDsCgccpf("");
	}

	public String getPessDsCgccpf() {
		return pessdscgccpf;
	}

	public void setPessDsCgccpf(String pessdscgccpf) {
		this.pessdscgccpf = pessdscgccpf;
	}
	
}


