package br.com.plusoft.csi.espec.plugin;

import com.plusoft.plugin.interfaces.PlusoftPluginValidator;

import br.com.plusoft.csi.espec.constantes.PermissaoEspec;
import br.com.plusoft.fw.log.Log;

public class ValidatorPlugin implements PlusoftPluginValidator {

	public void validatePlugin() throws NoClassDefFoundError{
		try{
			PermissaoEspec perm = new PermissaoEspec();
			Log.log(this.getClass(), Log.INFOPLUS, "Espec Validado!");
		}catch (Exception e){
			throw new NoClassDefFoundError("");
		}
	}

}
