package br.com.plusoft.csi.espec.adm.ejb;
/**
 * Home interface for Enterprise Bean: MAFinderEspecHelper
 */
public interface MAFinderEspecHelperHome extends javax.ejb.EJBHome {
	
	public MAFinderEspecHelper create()
			throws javax.ejb.CreateException, java.rmi.RemoteException;
}
