/*
 * Created on 31/05/2005
 */
package br.com.plusoft.csi.espec.constantes;

/**
 * Classe de constantes que contem as constantes especificas para o projeto
 * (cadastro)
 * 
 * @author victor.iberia
 */
public class ConstantesEspec {
	public static final long EMPRESA_PADRAO = 1;
	
	static private final String PACKAGE_ENTITY_ADM = "br/com/plusoft/csi/adm/dao/xml/"; 
	static private final String PACKAGE_ENTITY_CRM = "br/com/plusoft/csi/crm/dao/xml/"; 
	
	static private final String PACKAGE_ENTITY_ADM_ESPEC = "br/com/plusoft/csi/espec/adm/dao/xml/"; 
	static private final String PACKAGE_ENTITY_CRM_ESPEC = "br/com/plusoft/csi/espec/crm/dao/xml/"; 
		
}