/*
 * Created on 31/05/2005
 */
package br.com.plusoft.csi.espec.constantes;

import java.util.Vector;

import br.com.plusoft.csi.adm.helper.PermissaoConst;

/**
 * Classe que contem as constantes de permissionamento especificas do projeto Espec 
 */
public class PermissaoEspec extends PermissaoConst{
	public static final int GRUPO_FUNCIONALIDADE_CADASTRO_ESPEC = 10001;

	
	// Cadastro de Exemplo
	public static final int FUNCIONALIDADE_CADASTRO_EXEMPLO_ESPEC = 10001;	
	public static final String FUNCIONALIDADE_CADASTRO_EXEMPLO_INCLUSAO_CHAVE = "adm.espec.exemplo.inclusao";
	public static final String FUNCIONALIDADE_CADASTRO_EXEMPLO_ALTERACAO_CHAVE = "adm.espec.exemplo.alteracao";
	public static final String FUNCIONALIDADE_CADASTRO_EXEMPLO_EXCLUSAO_CHAVE = "adm.espec.exemplo.exclusao";
	public static final String FUNCIONALIDADE_CADASTRO_EXEMPLO_VISUALIZACAO_CHAVE = "adm.espec.exemplo.visualizacao";
	// Fim Cadastro 	
	
	
	

	public static void generateSql(){

		Vector sql = new Vector();

		//Cadastro grupo 
		sql.add("/* **GRUPO ESPEC ** */");
		sql.add("INSERT INTO CS_DMTB_GRUPOFUNCIO_GRFU (ID_GRFU_CD_GRUPOFUNCIO, ID_MODU_CD_MODULO, GRFU_DS_GRUPOFUNCIO, GRFU_IN_INATIVO, GRFU_DH_INATIVO) VALUES(" + GRUPO_FUNCIONALIDADE_CADASTRO_ESPEC +  "," + MODULO_CODIGO_CADASTRO + ",'Espec Padr�o', 'N', NULL);");
		sql.add("\n");
						
		//Cadastro 
		sql.add("/* **FUNCIONALIDADE Cadastro - Linha X Resposta Tabulada** */");
		sql.add("INSERT INTO CS_CDTB_FUNCIONALIDADE_FUCI (ID_FUCI_CD_FUNCIONALIDADE, ID_GRFU_CD_GRUPOFUNCIO, FUCI_DS_FUNCIOINTERNA, FUCI_DS_FUNCIONALIDADE, FUCI_DH_INATIVO, FUCI_DS_DEPENDS, FUCI_TX_OBSERVACAO) VALUES(" + FUNCIONALIDADE_CADASTRO_EXEMPLO_ESPEC + "," + GRUPO_FUNCIONALIDADE_CADASTRO_ESPEC + ",'Linha X Resp.Tabulada', 'Linha X Resp.Tabulada', NULL, NULL, 'Linha X Resp.Tabulada'); "); 
		sql.add("INSERT INTO CS_ASTB_FUNCIOPENI_FUPN (ID_FUCI_CD_FUNCIONALIDADE, ID_PERM_CD_PERMISSAO, ID_NIPE_CD_NIVELPERMISSAO, FUPN_DS_FUNCIOPENI) VALUES(" + FUNCIONALIDADE_CADASTRO_EXEMPLO_ESPEC + "," + PERMISSAO_CODIGO_INCLUSAO + "," + NIVELPERMISSAO_CODIGO_UNICO + ",'" + FUNCIONALIDADE_CADASTRO_EXEMPLO_INCLUSAO_CHAVE + "');");
		sql.add("INSERT INTO CS_ASTB_FUNCIOPENI_FUPN (ID_FUCI_CD_FUNCIONALIDADE, ID_PERM_CD_PERMISSAO, ID_NIPE_CD_NIVELPERMISSAO, FUPN_DS_FUNCIOPENI) VALUES(" + FUNCIONALIDADE_CADASTRO_EXEMPLO_ESPEC + "," + PERMISSAO_CODIGO_ALTERACAO + "," + NIVELPERMISSAO_CODIGO_UNICO + ",'" + FUNCIONALIDADE_CADASTRO_EXEMPLO_ALTERACAO_CHAVE + "');");
		sql.add("INSERT INTO CS_ASTB_FUNCIOPENI_FUPN (ID_FUCI_CD_FUNCIONALIDADE, ID_PERM_CD_PERMISSAO, ID_NIPE_CD_NIVELPERMISSAO, FUPN_DS_FUNCIOPENI) VALUES(" + FUNCIONALIDADE_CADASTRO_EXEMPLO_ESPEC + "," + PERMISSAO_CODIGO_EXCLUSAO + "," + NIVELPERMISSAO_CODIGO_UNICO + ",'" + FUNCIONALIDADE_CADASTRO_EXEMPLO_EXCLUSAO_CHAVE + "');");
		sql.add("INSERT INTO CS_ASTB_FUNCIOPENI_FUPN (ID_FUCI_CD_FUNCIONALIDADE, ID_PERM_CD_PERMISSAO, ID_NIPE_CD_NIVELPERMISSAO, FUPN_DS_FUNCIOPENI) VALUES(" + FUNCIONALIDADE_CADASTRO_EXEMPLO_ESPEC + "," + PERMISSAO_CODIGO_VISUALIZACAO + "," + NIVELPERMISSAO_CODIGO_UNICO + ",'" + FUNCIONALIDADE_CADASTRO_EXEMPLO_VISUALIZACAO_CHAVE + "');");
		sql.add("/* **FIM FUNCIONALIDADE Cadastro - Linha X Resposta Tabulada** */");
		sql.add("\n");
		//Fim Cadastro de Motivo do Ultimo Contato

		for (int i = 0; i < sql.size(); i++){
			System.out.println(sql.get(i));
		}
	}
	
	public static void main(String args[]){
		generateSql();
	}

}
